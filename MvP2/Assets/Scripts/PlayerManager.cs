﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public float acceleration = 2;
    public float maxSpeed = 10;
    public int money = 0;
    public Text moneyText;
    public Text forgesText;

    public GameObject forgePrefab;

    public bool gameOver = false;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        speed = 0;
        SetMoneyText();
        SetForgeText();
    }
	
	// Update is called once per frame
	void Update()
    {
        if (!gameOver)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                maxSpeed = 30;
            }
            else
            {
                maxSpeed = 10;
            }
            if (Input.GetAxis("Vertical") != 0)
            {
                speed += acceleration * Input.GetAxis("Vertical");
            }
            if (speed > maxSpeed)
            {
                speed = maxSpeed;
            }
            if (speed < -maxSpeed)
            {
                speed = -maxSpeed;
            }
            if (Input.GetAxis("Vertical") == 0)
            {
                speed = 0;
            }
            rb.velocity = new Vector3(transform.forward.x * speed, rb.velocity.y, transform.forward.z * speed);
            if (Input.GetAxis("Horizontal") != 0)
            {
                transform.Rotate(transform.up, 1.5f * acceleration * Input.GetAxis("Horizontal"));
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                if (money >= 100)
                {
                    money -= 100;
                    SetMoneyText();
                    Vector3 forgePos = transform.position + transform.forward * 10;
                    forgePos.y = 30 * Chunk.GetHeight(forgePos.x / Chunk.m_size, forgePos.z / Chunk.m_size);
                    GameObject g = Instantiate(forgePrefab, forgePos, Quaternion.Euler(270, 0, 0));
                    g.transform.localScale = new Vector3(150f, 150f, 150f);
                    Vector2 coords = Chunk.GetChunkCoordinates(transform.position.x, transform.position.z);
                    Chunk parent;
                    if (World.instance.instantiatedChunks.TryGetValue(coords, out parent))
                    {
                        g.transform.SetParent(parent.transform);
                    }
                    World.instance.forgesBuilt++;
                    SetForgeText();
                    if (!World.instance.forges.ContainsKey(Chunk.GetChunkIndex(transform.position.x, transform.position.z)))
                    {
                        World.instance.forges.Add(Chunk.GetChunkIndex(transform.position.x, transform.position.z), new List<Vector2>());
                    }

                    World.instance.forges[Chunk.GetChunkIndex(transform.position.x, transform.position.z)].Add(new Vector2((transform.position.x - coords.x) / Chunk.m_size, (transform.position.z - coords.y) / Chunk.m_size));
                }
            }
        }
    }

    public void PickupTreasure(int money)
    {
        this.money += money;
        SetMoneyText();
    }

    private void SetMoneyText()
    {
        moneyText.text = "Gold: " + money.ToString();
    }

    private void SetForgeText()
    {
        forgesText.text = "Forges built: " + World.instance.forgesBuilt.ToString();
    }
}
