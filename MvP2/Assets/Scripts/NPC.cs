﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour
{
    public Vector3 target;
    public NavMeshAgent agent;
    public GameObject werewolfPrefab;

	// Use this for initialization
	void Start()
    {
        target = transform.position;
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update()
    {
        Vector3 playerPos = Camera.main.transform.position;
        Vector2 myPos = new Vector2(transform.position.x, transform.position.z);
        float dist = Vector2.Distance(myPos, new Vector2(playerPos.x, playerPos.z));
        if (Mathf.Abs(dist) > World.instance.m_visibleWorldRadius * 3 * Chunk.m_size)
        {
            Destroy(gameObject);
        }
        if (gameObject.tag.Equals("Monster"))
        {
            if (agent.isOnNavMesh && agent.velocity.magnitude == 0)
            {
                GameObject p = FindClosest("Peasant");
                agent.SetDestination(p.transform.position);
            }
        }
        if (gameObject.tag.Equals("Peasant"))
        {
            if (agent.isOnNavMesh && agent.velocity.magnitude == 0)
            {
                GameObject v = GameObject.FindGameObjectWithTag("Village");
                if (v != null && Random.value < 0.4f)
                {
                    agent.SetDestination(v.transform.position);
                }
                else
                {
                    agent.SetDestination(GetRandomTarget());
                }
            }
        }
        if (gameObject.tag.Equals("Armed"))
        {
            if (agent.isOnNavMesh)
            {
                GameObject w = FindClosest("Monster");
                target = w.transform.position;
                agent.SetDestination(target);
            }
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (tag.Equals("Monster"))
        {
            if (collision.gameObject.tag.Equals("Peasant"))
            {
                Vector3 p = collision.gameObject.transform.position;
                Destroy(collision.gameObject);
                Instantiate(werewolfPrefab, p, Quaternion.identity);
                Debug.Log("Bouffe");
            }
            if (collision.gameObject.tag.Equals("Armed"))
            {
                if (Random.value <= 0.5f) // The werewolfs transforms the armed peasant into a werewolf
                {
                    Vector3 p = collision.gameObject.transform.position;
                    Destroy(collision.gameObject);
                    Instantiate(werewolfPrefab, p, Quaternion.identity);
                }
                else // The armed peasant kills the werewolf
                {
                    Destroy(gameObject);
                }
            }
            if (collision.gameObject.tag.Equals("Player"))
            {
                Destroy(gameObject);
            }
        }
    }

    Vector3 GetRandomTarget()
    {
        float x = transform.position.x + (Random.value - 0.5f) * Random.value * Chunk.m_size;
        float y = transform.position.z + (Random.value - 0.5f) * Random.value * Chunk.m_size;
        Vector3 ret = new Vector3(x, 30 * Chunk.GetHeight(x, y), y);
        return ret;
    }

    public GameObject FindClosest(string tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(tag);
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }
}
