﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpTreasure : MonoBehaviour
{
    public int money = 200;
    public GameObject openTreasure;

    private bool m_hasCollided = false;

	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player") && !m_hasCollided)
        {
            m_hasCollided = true;
            collision.gameObject.GetComponent<PlayerManager>().PickupTreasure(money);
            Vector2Int labPos = Chunk.GetChunkIndex(collision.transform.position.x, collision.transform.position.z);
            if (collision.transform.position.x < 0) { labPos.x = (int)Chunk.Nfmod(labPos.x - 1, World.instance.m_worldSize); }
            if (collision.transform.position.z < 0) { labPos.y = (int)Chunk.Nfmod(labPos.y - 1, World.instance.m_worldSize); }
            World.instance.chunksWithLabyrinth[labPos] = true;
            GameObject g = Instantiate(openTreasure, transform.position, Quaternion.identity);
            Vector2 coords = Chunk.GetChunkCoordinates(transform.position.x, transform.position.z);
            Chunk parent;
            if (World.instance.instantiatedChunks.TryGetValue(coords, out parent))
            {
                g.transform.SetParent(parent.transform);
            }
            Destroy(gameObject);
        }
    }
}
