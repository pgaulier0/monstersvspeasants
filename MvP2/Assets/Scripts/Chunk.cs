﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    [Range(2, 100)]
    public static int m_samples = 100; // Number of points in the length of the chunk
    public static int m_size = 100; // Length and Width of the chunk
    public int m_xIndex; // Index of the chunk in x within the world, 
                         // from 0 to worldSize - 1
    public int m_yIndex; // Index of the chunk in y within the world,
                         // from 0 to worldSize - 1

    /* Coordinates in a chunk AND in the world
    * y
    * (z)
    * /\
    * |
    * |
    * |
    * |
    *   ------------> x (x)
    * 
    */

    public float m_posx;
    public float m_posy;

    public int numberOfVillages = 0;

    private float[,] m_map = null;
    private Mesh m_terrain = null;

    public GameObject m_treePrefab;
    public GameObject m_housePrefab;
    public GameObject m_labyrinthPrefab;
    public GameObject m_forgePrefab;
    public GameObject m_villagerPrefab;
    public GameObject m_armedVillagerPrefab;
    public GameObject m_werewolfPrefab;

    public DiseaseSpread ds;

    public bool hasLabyrinth;
    private int m_labyrinthOffset;
    private float m_maxHeight;

	// Use this for initialization
	void Start()
    {
        // Find out what xIndex and yIndex are
        float x = transform.position.x;
        float y = transform.position.z;
        Vector2Int chunkIndex = GetChunkIndex(x, y);
        m_xIndex = chunkIndex.x;
        m_yIndex = chunkIndex.y;

        m_posx = x / m_size;
        m_posy = y / m_size;

        if (hasLabyrinth)
        {
            LabyrinthPrim lp = m_labyrinthPrefab.GetComponent<LabyrinthPrim>();
            float size = lp.GetCellSize() * lp.size;
            m_labyrinthOffset = Mathf.FloorToInt((((m_size - size) / 2) / m_size) * m_samples);
        }
    
        // Build terrain
        m_map = GenerateHeightMap();
        m_terrain = GenerateTerrain(m_map);
        GenerateTrees();
        GenerateLabyrinth();
        GenerateVillages();
        ds = Camera.main.GetComponent<DiseaseSpread>();
        GenerateForges();
        GenerateNPCs();
    }
	
	// Update is called once per frame
	void Update()
    {
        Vector3 playerPos = Camera.main.transform.position;
        Vector2 myPos = new Vector2(transform.position.x, transform.position.z);
        float dist = Vector2.Distance(myPos, new Vector2(playerPos.x, playerPos.z));
        if (Mathf.Abs(dist) > World.instance.m_visibleWorldRadius * 3 * m_size)
        {
            Chunk myself;
            bool found = World.instance.instantiatedChunks.TryGetValue(myPos, out myself);
            if (found)
            {
                World.instance.instantiatedChunks.Remove(myPos);
                Destroy(gameObject);
            }
        }
    }

    public static Vector2Int GetChunkIndex(float x, float y)
    {
        int xi = (int)Nfmod(x - x % m_size, (World.instance.m_worldSize * m_size)) / m_size;
        int yi = (int)Nfmod(y - y % m_size, (World.instance.m_worldSize * m_size)) / m_size;
        return new Vector2Int(xi, yi);
    }

    public static Vector2 GetChunkCoordinates(float x, float y)
    {
        float xi, yi;
        if (x > 0)
        {
            xi = x - x % m_size;
        }
        else
        {
            xi = x - x % m_size - m_size;
        }
        if (y > 0)
        {
            yi = y - y % m_size;
        }
        else
        {
            yi = y - y % m_size - m_size;
        }
        return new Vector2(xi, yi);
    }

    private float[,] GenerateHeightMap()
    {
        float[,] map = new float[m_samples, m_samples];
        for (int i = 0; i < m_samples; i++)
        {
            for (int j = 0; j < m_samples; j++)
            {
                if (hasLabyrinth && i > m_labyrinthOffset && i < m_size - m_labyrinthOffset
                    && j > m_labyrinthOffset && j < m_size - m_labyrinthOffset)
                {
                    map[i, j] = 0;
                }
                else
                {
                    float x = m_posx % World.instance.m_worldSize + i / (float)(m_samples - 1);
                    float y = m_posy % World.instance.m_worldSize + j / (float)(m_samples - 1);
                    map[i, j] = 30 * GetHeight(x, y);
                    if (map[i, j] > m_maxHeight)
                    {
                        m_maxHeight = map[i, j];
                    }
                }
            }
        }
        return map;
    }

    public static float GetHeight(float x, float y)
    {
        float alpha = (World.instance.m_worldSize - x) / World.instance.m_worldSize;
        float beta = (World.instance.m_worldSize - y) / World.instance.m_worldSize;

        float x2 = World.instance.m_worldSize - x;
        float y2 = World.instance.m_worldSize - y;

        return (alpha * beta * Mathf.PerlinNoise(x + 10000, y + 10000)
                    + (1 - alpha) * beta * Mathf.PerlinNoise(x2 + 10000, y + 10000)
                    + alpha * (1 - beta) * Mathf.PerlinNoise(x + 10000, y2 + 10000)
                    + (1 - alpha) * (1 - beta) * Mathf.PerlinNoise(x2 + 10000, y2 + 10000));
    }

    public static float Nfmod(float a, float b)
    {
        return a - b * Mathf.Floor(a / b);
    }

    // Create a terrain from the heightmap
    private Mesh GenerateTerrain(float[,] map)
    {
        float rsample = 1 / (float)(m_samples - 1);
        Vector3[] vertices;
        Vector2[] uv;
        int[] triangles;
        int ti = 0;
        int vi = 0;
        Mesh terrain = new Mesh();

        vertices = new Vector3[m_samples * m_samples];
        uv = new Vector2[vertices.Length];

        triangles = new int[(m_samples - 1) * (m_samples - 1) * 6];
        for (int i = 0, y = 0; y < m_samples; y++)
        {
            for (int x = 0; x < m_samples; x++, i++)
            {
                vertices[i] = new Vector3(x * m_size * rsample, map[x, y], y * m_size * rsample);
                uv[i] = new Vector2((m_posx % World.instance.m_worldSize + x / (float)(m_samples - 1)) / World.instance.m_worldSize,
                    (m_posy % World.instance.m_worldSize + y / (float)(m_samples - 1)) / World.instance.m_worldSize);
            }
        }
        for (int y = 0; y < m_samples - 1; y++)
        {
            for (int x = 0; x < m_samples - 1; x++)
            {
                triangles[ti] = vi;
                triangles[ti + 1] = vi + m_samples;
                triangles[ti + 2] = vi + m_samples + 1;
                triangles[ti + 3] = vi;
                triangles[ti + 4] = vi + m_samples + 1;
                triangles[ti + 5] = vi + 1;
                ti += 6;
                vi++;
            }
            vi++;
        }
        terrain.vertices = vertices;
        terrain.triangles = triangles;
        terrain.uv = uv;
        terrain.RecalculateNormals();
        GetComponent<MeshCollider>().sharedMesh = terrain;
        GetComponent<MeshFilter>().mesh = terrain;
        return terrain;
    }

    void GenerateTrees()
    {
        for (int i = 0; i < m_samples; i++)
        {
            for (int j = 0; j < m_samples; j++)
            {
                if (!hasLabyrinth || i < m_labyrinthOffset && i > m_size - m_labyrinthOffset
                    && j < m_labyrinthOffset && j > m_size - m_labyrinthOffset)
                {
                    float x = m_posx % World.instance.m_worldSize + i / (float)(m_samples - 1);
                    float y = m_posy % World.instance.m_worldSize + j / (float)(m_samples - 1);
                    if (Mathf.PerlinNoise(x * 84.4f, y * 17.3f) > 0.95)
                    {
                        GameObject g = Instantiate(m_treePrefab, new Vector3(transform.position.x + i,
                            m_map[i, j],
                            transform.position.z + j), Quaternion.identity);
                        g.transform.SetParent(this.transform);
                        i += 3; j += 3;
                    }
                }
            }
        }
    }

    void GenerateVillages()
    {
        if (!hasLabyrinth)
        {
            Vector2Int index = GetChunkIndex(transform.position.x, transform.position.z);
            List<Vector2> l = World.instance.villages[index];
            foreach (Vector2 v in l)
            {
                numberOfVillages++;
                GameObject g = Instantiate(m_housePrefab,
                    new Vector3(transform.position.x + v.x * m_size, m_map[Mathf.FloorToInt(v.x * m_samples), Mathf.FloorToInt(v.y * m_samples)],
                    transform.position.z + v.y * m_size),
                    Quaternion.Euler(new Vector3(0, Random.value * 360, 0)));
                g.tag = "Village";
                g.transform.localScale = new Vector3(6, 6, 6);
                g.transform.SetParent(transform);
            }
        }
    }

    void GenerateLabyrinth()
    {
        if (hasLabyrinth)
        {
            int randRotation = Mathf.FloorToInt(Random.value * 4) * 90;
            GameObject g = Instantiate(m_labyrinthPrefab, transform.position, Quaternion.Euler(0, randRotation, 0));
            g.GetComponent<LabyrinthPrim>().position = new Vector3(transform.position.x + m_labyrinthOffset, m_maxHeight + 2.5f, transform.position.z + m_labyrinthOffset);
            g.GetComponent<LabyrinthPrim>().rndSeed = Mathf.FloorToInt(100 * Mathf.PerlinNoise(316.146f * m_xIndex + 13.618f, 618.614f * m_yIndex + 87.369f));
            g.transform.SetParent(transform);
        }
    }

    void GenerateNPCs()
    {
        if (!hasLabyrinth)
        {
            int worldSize = World.instance.m_worldSize;
            int s = (int)(ds.curSane * ds.originalPopulation / (worldSize * worldSize));
            float a = (int)(ds.curArmed * ds.originalPopulation / (worldSize * worldSize));
            float w = (int)(ds.curWerewolves * ds.originalPopulation / (worldSize * worldSize));
            if (s == 0 && Random.value <= ds.curSane)
            {
                int p1 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(a + 64, w + 5));
                int p2 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(w + 14, w + 32));
                float x = m_posx % worldSize + p1 / (float)(m_samples - 1);
                float y = m_posy % worldSize + p2 / (float)(m_samples - 1);
                Instantiate(m_villagerPrefab, new Vector3(transform.position.x + p1, m_map[p1, p2] + 1, transform.position.z + p2), Quaternion.identity);
            }
            if (a == 0 && Random.value <= ds.curArmed)
            {
                int p1 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(s + 4, s + 19));
                int p2 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(a + 87, s + 17));
                float x = m_posx % worldSize + p1 / (float)(m_samples - 1);
                float y = m_posy % worldSize + p2 / (float)(m_samples - 1);
                Instantiate(m_armedVillagerPrefab, new Vector3(transform.position.x + p1, m_map[p1, p2] + 1, transform.position.z + p2), Quaternion.identity);
            }
            if (w == 0 && Random.value <= ds.curWerewolves)
            {
                int p1 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(w + 14, a + 73));
                int p2 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(s + 12, w + 75));
                float x = m_posx % worldSize + p1 / (float)(m_samples - 1);
                float y = m_posy % worldSize + p2 / (float)(m_samples - 1);
                Instantiate(m_werewolfPrefab, new Vector3(transform.position.x + p1, m_map[p1, p2] + 1, transform.position.z + p2), Quaternion.identity);
            }
            for (int i = 0; i < s; i++)
            {
                int p1 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(a + 63, a + 3));
                int p2 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(a + 8, w + 46));
                float x = m_posx % worldSize + p1 / (float)(m_samples - 1);
                float y = m_posy % worldSize + p2 / (float)(m_samples - 1);
                Instantiate(m_villagerPrefab, new Vector3(transform.position.x + p1, m_map[p1, p2] + 1, transform.position.z + p2), Quaternion.identity);
            }
            for (int i = 0; i < a; i++)
            {
                int p1 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(s + 27, a + 56));
                int p2 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(a + 81, a + 96));
                float x = m_posx % worldSize + p1 / (float)(m_samples - 1);
                float y = m_posy % worldSize + p2 / (float)(m_samples - 1);
                Instantiate(m_armedVillagerPrefab, new Vector3(transform.position.x + p1, m_map[p1, p2] + 1, transform.position.z + p2), Quaternion.identity);
            }
            for (int i = 0; i < w; i++)
            {
                int p1 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(w + 71, s + 55));
                int p2 = Mathf.FloorToInt((m_samples - 1) * Mathf.PerlinNoise(a + 26, s + 51));
                float x = m_posx % worldSize + p1 / (float)(m_samples - 1);
                float y = m_posy % worldSize + p2 / (float)(m_samples - 1);
                Instantiate(m_werewolfPrefab, new Vector3(transform.position.x + p1, m_map[p1, p2] + 1, transform.position.z + p2), Quaternion.identity);
            }
        }
    }

    void GenerateForges()
    {
        Vector2Int index = GetChunkIndex(transform.position.x, transform.position.z);
        if (World.instance.forges.ContainsKey(index))
        {
            List<Vector2> l = World.instance.forges[index];
            foreach (Vector2 v in l)
            {
                GameObject g = Instantiate(m_forgePrefab,
                    new Vector3(transform.position.x + v.x * m_size, m_map[Mathf.FloorToInt(v.x * m_samples), Mathf.FloorToInt(v.y * m_samples)],
                    transform.position.z + v.y * m_size),
                    Quaternion.Euler(new Vector3(270, 0, 0)));
                g.transform.localScale = new Vector3(150f, 150f, 150f);
                g.transform.SetParent(transform);
            }
        }
    }
}

