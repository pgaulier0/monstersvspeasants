using UnityEngine;
using System.Collections.Generic;
using Delaunay;
using Delaunay.Geo;

public class VoronoiDemo : MonoBehaviour
{
	private int	m_pointCount = 500;
	public Material land;
	private List<Vector2> m_points;
    public static int pixelsPerChunk = 16;
	private int m_mapWidth;
	private int m_mapHeight;
	private List<LineSegment> m_edges = null;
	private List<LineSegment> m_spanningTree;
	private List<LineSegment> m_delaunayTriangulation;
	public Texture2D tx;
	private Color[] pixels;
	float [,] map;
    private Dictionary<Vector2, int> m_nodesDegrees;

    /* Generate a heightmap */
    float[,] Createmap()
    {
		float[,] map = new float[m_mapWidth, m_mapHeight];
        for (int i = 0; i < m_mapWidth; i++)
        {
            for (int j = 0; j < m_mapHeight; j++)
            {
                map[i, j] = Chunk.GetHeight(i * Chunk.m_size, j * Chunk.m_size);
            }
        }
		return map;
	}

	/* Create a random point */
	Vector2 Findpoint(float [,] map)
    {
		int x, y;
        int i = 0;
        do
        {
            x = Random.Range(0, m_mapWidth);
            y = Random.Range(0, m_mapHeight);
            i++;
        } while (map[x, y] > 0.6f || i < 5);
		return new Vector2 (x, y);
	}

	void Start()
	{
        m_mapHeight = World.instance.m_worldSize * pixelsPerChunk;
        m_mapWidth = World.instance.m_worldSize * pixelsPerChunk;
		map = Createmap();
        m_nodesDegrees = new Dictionary<Vector2, int>();
		tx = new Texture2D(m_mapWidth, m_mapHeight);
		pixels = new Color[m_mapWidth * m_mapHeight];
        for (int i = 0; i < m_mapWidth; i++)
        {
            for (int j = 0; j < m_mapHeight; j++)
            {
                pixels[i * m_mapHeight + j] = Color.Lerp(Color.green, Color.HSVToRGB(63 / 255f, 45 / 255f, 41 / 255f), map[i, j]);
            }
        }
		m_points = new List<Vector2>();
		List<uint> colors = new List<uint>();
        /* Randomly pick vertices */
        Vector2 dx = new Vector2(m_mapWidth, 0);
        Vector2 dy = new Vector2(0, m_mapHeight);
		for (int i = 0; i < m_pointCount; i++)
        {
			Vector2 vec = Findpoint(map);
			m_points.Add(vec);
            colors.Add(0);
            m_points.Add(vec + dx);
            colors.Add(0);
            m_points.Add(vec + dy);
            colors.Add(0);
            m_points.Add(vec - dx);
            colors.Add(0);
            m_points.Add(vec - dy);
            colors.Add(0);
        }
		/* Generate Graphs */
		Voronoi v = new Voronoi(m_points, colors, new Rect(0, 0, m_mapWidth, m_mapHeight));
		m_edges = v.VoronoiDiagram();
		m_spanningTree = v.SpanningTree(KruskalType.MINIMUM);
		m_delaunayTriangulation = v.DelaunayTriangulation();

        for (int i = 0; i < m_spanningTree.Count; i++)
        {
            LineSegment seg = m_spanningTree[i];
            Vector2 left = (Vector2)seg.p0;
            Vector2 right = (Vector2)seg.p1;
            if (!m_nodesDegrees.ContainsKey(left))
            {
                m_nodesDegrees.Add(left, 1);
            }
            else
            {
                m_nodesDegrees[left] = m_nodesDegrees[left] + 1;
            }
            if (!m_nodesDegrees.ContainsKey(right))
            {
                m_nodesDegrees.Add(right, 1);
            }
            else
            {
                m_nodesDegrees[right] = m_nodesDegrees[right] + 1;
            }
        }

        /* Shows spanning tree */
        Color color = Color.blue;
		if (m_spanningTree != null)
        {
			for (int i = 0; i < m_spanningTree.Count; i++)
            {
				LineSegment seg = m_spanningTree[i];				
				Vector2 left = (Vector2)seg.p0;
				Vector2 right = (Vector2)seg.p1;
				DrawLine(left, right, color);

                if (left.x >= 0 && left.x < m_mapWidth && left.y >= 0 && left.y < m_mapHeight)
                {
                    if (m_nodesDegrees[left] == 1 && Random.value <= 0.8)
                    {
                        DrawPoint(left, Color.red);
                        Vector2[] coords = TexToWorld(left);
                        Vector2Int key = new Vector2Int((int)coords[0].x, (int)coords[0].y);
                        World.instance.villages[key].Add(coords[1]);
                    }
                }
			}
		}
		land.SetTexture("_SplatTex", tx);
		tx.SetPixels(pixels);
		tx.Apply();

        SpawnLabyrinths();
	}

	private void DrawPoint (Vector2 p, Color c)
    {
        if (p.x < m_mapWidth && p.x >= 0 && p.y < m_mapHeight && p.y >= 0)
        {
            pixels[(int)p.x * m_mapHeight + (int)p.y] = c;
        }
	}

	private bool Isinside(Vector2 p, Vector2 q)
    {
		return ((p.x < m_mapWidth && p.x >= 0 && p.y < m_mapHeight && p.y >= 0) ||
		(q.x < m_mapWidth && q.x >= 0 && q.y < m_mapHeight && q.y >= 0));
	}

	// Bresenham line algorithm
	private void DrawLine(Vector2 p0, Vector2 p1, Color c)
    {
		int x0 = (int)p0.x;
		int y0 = (int)p0.y;
		int x1 = (int)p1.x;
		int y1 = (int)p1.y;

		int dx = Mathf.Abs(x1 - x0);
		int dy = Mathf.Abs(y1 - y0);
		int sx = x0 < x1 ? 1 : -1;
		int sy = y0 < y1 ? 1 : -1;
		int err = dx - dy;
        if (!Isinside(p0, p1))
        {
            return;
        }
		while (true)
        {
            if (x0 < m_mapWidth && x0 >= 0 && y0 < m_mapHeight && y0 >= 0)
            {
                pixels[x0 * m_mapHeight + y0] = c;
            }

			if (x0 == x1 && y0 == y1) break;
			int e2 = 2 * err;
			if (e2 > -dy)
            {
				err -= dy;
				x0 += sx;
			}
			if (e2 < dx)
            {
				err += dx;
				y0 += sy;
			}
		}
	}

    // First vector of the list is the index of the chunk, 
    // second one is the offset within the chunk (between 0 and 1).
    public Vector2[] TexToWorld(Vector2 texCoord)
    {
        Vector2[] ret = new Vector2[2];
        // Invert the coords because texture and world don't have the same axes
        texCoord = new Vector2(texCoord.y, texCoord.x);
        Vector2 index = new Vector2((int)texCoord.x / pixelsPerChunk, (int)texCoord.y / pixelsPerChunk);
        Vector2 offset = new Vector2(Chunk.Nfmod(texCoord.x, pixelsPerChunk) / pixelsPerChunk,
            Chunk.Nfmod(texCoord.y, pixelsPerChunk) / pixelsPerChunk);
        ret[0] = index;
        ret[1] = offset;
        return ret;
    }

    private void SpawnLabyrinths()
    {
        int i = 0;
        foreach (KeyValuePair<Vector2Int, List<Vector2>> kvp in World.instance.villages)
        {
            if (kvp.Value.Count == 0 && Random.value < 0.05) // If the chunk doesn't have any village
            {
                World.instance.chunksWithLabyrinth.Add(kvp.Key, false);
                i++;
            }
        }
    }
}