﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiseaseExample : MonoBehaviour
{
    public float weaponSpreadingRate = 0.1f;
    public float diseaseSpreadingRate = 0.1f;
    public float armedAgainstWerewolf = 0.1f;
    public float werewolfAgainstArmed = 0.05f;
    public float dt = 1;

    public float startingSane = 0.95f;
    public float startingArmed = 0f;
    public float startingWerewolves = 0.05f;
    public float curSane;
    public float curArmed;
    public float curWerewolves;

    public int originalPopulation = 100;

    public Texture2D tx;
    public Material mat;

    private int SIZE = 600;
    private Color[] pixels;
    private float weaponsTimesForges;

    // Use this for initialization
    void Start()
    {
        curSane = startingSane;
        curArmed = startingArmed;
        curWerewolves = startingWerewolves;
        tx = new Texture2D(SIZE, SIZE);
        pixels = new Color[SIZE * SIZE];

        float s = curSane;
        float a = curArmed;
        float w = curWerewolves;
        float s1, a1, w1;

        for (int x = 0; x < SIZE; x++)
        {
            for (int y = 0; y < SIZE; y++)
            {
                DrawPoint(new Vector2(y, x), Color.white);
            }
        }
        weaponsTimesForges = 2 * weaponSpreadingRate;
        for (int i = 0; i < SIZE; i++)
        {
            s1 = s + (-weaponsTimesForges * s - diseaseSpreadingRate * s * w) * dt;
            a1 = a + (weaponsTimesForges * s - werewolfAgainstArmed * a * w) * dt;
            w1 = w + (diseaseSpreadingRate * s * w
               + werewolfAgainstArmed * a * w
               - armedAgainstWerewolf * a * w) * dt;
            s = curSane = s1;
            a = curArmed = a1;
            w = curWerewolves = w1;

            DrawPoint(new Vector2(s * SIZE, i), Color.green);
            DrawPoint(new Vector2(a * SIZE, i), Color.blue);
            DrawPoint(new Vector2(w * SIZE, i), Color.red);
        }

        mat.SetTexture("_MainTex", tx);
        tx.SetPixels(pixels);
        tx.Apply();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void DrawPoint(Vector2 p, Color c)
    {
        if (p.x < SIZE && p.x >= 0 && p.y < SIZE && p.y >= 0)
            pixels[(int)p.x * SIZE + (int)p.y] = c;
    }
}
