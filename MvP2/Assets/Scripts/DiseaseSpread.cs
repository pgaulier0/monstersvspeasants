﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiseaseSpread : MonoBehaviour
{
    public float weaponSpreadingRate = 0.1f;
    public float diseaseSpreadingRate = 0.1f;
    public float armedAgainstWerewolf = 0.1f;
    public float werewolfAgainstArmed = 0.05f;
    public float dt = 1;

    public float startingSane = 0.95f;
    public float startingArmed = 0f;
    public float startingWerewolves = 0.05f;
    public float curSane;
    public float curArmed;
    public float curWerewolves;

    public int originalPopulation = 100;

    private float weaponsTimesForges;
    private float armedWeapons;

    private float lastTime;

    public Text gameOverText;

    // Use this for initialization
    void Start()
    {
        curSane = startingSane;
        curArmed = startingArmed;
        curWerewolves = startingWerewolves;
        lastTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        weaponsTimesForges = World.instance.forgesBuilt * weaponSpreadingRate;
        armedWeapons = armedAgainstWerewolf + World.instance.forgesBuilt * 0.02f;
        if (Time.time - lastTime >= 10)
        {
            float s = curSane;
            float a = curArmed;
            float w = curWerewolves;
            float s1, a1, w1;

            s1 = s + (-weaponsTimesForges * s - diseaseSpreadingRate * s * w) * dt;
            a1 = a + (weaponsTimesForges * s - werewolfAgainstArmed * a * w) * dt;
            w1 = w + (diseaseSpreadingRate * s * w
               + werewolfAgainstArmed * a * w
               - armedAgainstWerewolf * a * w) * dt;
            s = curSane = s1;
            a = curArmed = a1;
            w = curWerewolves = w1;

            lastTime = Time.time;
        }
        if ((curArmed + curSane) * originalPopulation < 1) // Lose
        {
            gameOverText.text = "You Lose.\nAll the peasants have been transformed into werewolves.";
            gameObject.GetComponentInParent<PlayerManager>().gameOver = true;
        }
        if (curWerewolves * originalPopulation < 1) // Win
        {
            gameOverText.text = "You win.\nThe werewolves have been eradicated.";
            gameObject.GetComponentInParent<PlayerManager>().gameOver = true;
        }
    }
}
