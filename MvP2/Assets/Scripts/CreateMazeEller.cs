﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class CellEller
{
    public int value = -1;
    public GameObject floor;
    public GameObject[] wall;
    public bool[] door;
}

public class CreateMazeEller : MonoBehaviour
{
    public GameObject floor, door; /* Two cubes, centered in 0, with y scale = 0.1. This can be the same prefab. */
    public int doors = 80; /* Number of tentative doors */
    public static int SIZE = 20;

    CellEller[,] maze;
    int row = 0;
    int count = 0, phase = 0;
    int j = 0;
    bool through = false;
    static int WEST = 1, NORTH = 0;

    // Use this for initialization
    void Start()
    {
        maze = new CellEller[SIZE, SIZE];
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++, count++)
            {
                maze[i, j] = new CellEller();
                maze[i, j].value = count % 200;
                maze[i, j].door = new bool[2] { false, false };
                maze[i, j].floor = Instantiate(floor, new Vector3(i, 0, j), Quaternion.Euler(0, 0, 0));
                maze[i, j].floor.GetComponent<Renderer>().material.color = color(maze[i, j].value);
                maze[i, j].wall = new GameObject[2] {
                    Instantiate (floor, new Vector3 (i - 0.5f, 0.5f, j), Quaternion.Euler (90, 90, 0)),
                    Instantiate (floor, new Vector3 (i, 0.5f, j - 0.5f), Quaternion.Euler (90, 0, 0))
                };

            }
    }
    Color color(float v)
    {
        return Color.HSVToRGB(v / 200.0f, 1f, 0.5f);
    }
    void breakthrough(int i, int j)
    {
        removeWall(i + 1, j, NORTH);
        maze[i + 1, j].value = maze[i, j].value;
        maze[i + 1, j].floor.GetComponent<Renderer>().material.color = color(maze[i, j].value);

    }
    void adddoor(int i, int j, int o)
    {
        if (maze[i, j].wall[o] != null)
            return;
        if (o == NORTH)
        { /* Do not add single cell room */
            if (i > 0 && j < SIZE - 1 && maze[i - 1, j].wall[o] != null && maze[i - 1, j].wall[WEST] != null && maze[i - 1, j + 1].wall[WEST] != null)
                return;
            if (i < SIZE - 1 && j < SIZE - 1 && maze[i + 1, j].wall[o] != null && maze[i, j].wall[WEST] != null && maze[i, j + 1].wall[WEST] != null)
                return;
        }
        if (o == WEST)
        {/* Do not add single cell room */
            if (i < SIZE - 1 && j > 0 && maze[i, j - 1].wall[o] != null && maze[i, j - 1].wall[NORTH] != null && maze[i + 1, j - 1].wall[NORTH] != null)
                return;
            if (i < SIZE - 1 && j < SIZE - 1 && maze[i, j + 1].wall[o] != null && maze[i, j].wall[NORTH] != null && maze[i + 1, j].wall[NORTH] != null)
                return;
        }
        if (o == NORTH) /* Add the door. Consider it as a wall */
            maze[i, j].wall[o] = Instantiate(door, new Vector3(i - 0.5f, 0.5f, j), Quaternion.Euler(90, 90, 0));
        else
            maze[i, j].wall[o] = Instantiate(door, new Vector3(i, 0.5f, j - 0.5f), Quaternion.Euler(90, 0, 0));
        maze[i, j].door[o] = true;
        maze[i, j].wall[o].GetComponent<Renderer>().material.color = Color.red;

    }
    void setvalue(int a, int b, int val)
    {
        maze[a, b].value = val;
        maze[a, b].floor.GetComponent<Renderer>().material.color = color(val);
    }
    void floodroom(int a, int b)
    {
        for (int k = 0; k <= row; k++)
            for (int h = 0; h < SIZE; h++)
                if (maze[k, h].value == a)
                    setvalue(k, h, b);
    }
    void removeWall(int i, int j, int o)
    {
        Destroy(maze[i, j].wall[o]);
        maze[i, j].wall[o] = null;
    }
    // Update is called once per frame
    void Update()
    {
        if (row >= SIZE)
        {
            phase++;
            row = 0;
            j = 0;
        }
        switch (phase)
        {
            case 0: /* Create maze, using Eller method */
                if (j >= 1 && maze[row, j - 1].value != maze[row, j].value && Random.value < 0.5f)
                {
                    int m = Mathf.Min(maze[row, j].value, maze[row, j - 1].value);
                    setvalue(row, j, m);
                    setvalue(row, j - 1, m);
                    removeWall(row, j, WEST);
                }
                if (row < SIZE - 1)
                {
                    if (j >= 1)
                    {
                        if (maze[row, j].value != maze[row, j - 1].value)
                        {
                            if (!through)
                                breakthrough(row, j - 1);
                            through = false;
                        }
                        else if (Random.value < 0.5f)
                        {
                            through = true;
                            breakthrough(row, j - 1);
                        }

                    }
                    if (j == SIZE - 1 && !through)
                    {
                        breakthrough(row, j);
                    }
                }
                else if (j >= 1 && maze[row, j].value != maze[row, j - 1].value)
                    removeWall(row, j, WEST);
                break;
            case 1:/* Add doors randomly */
                for (int i = 0; i < doors; i++)
                    adddoor(Random.Range(1, SIZE - 1), Random.Range(1, SIZE - 1), Random.Range(0, 2));
                count = 0;
                row = SIZE;
                break;
            case 2:  /* Find rooms */
                if (row > 0 && maze[row, j].wall[NORTH] == null)
                    setvalue(row, j, maze[row - 1, j].value);
                else
                    setvalue(row, j, count++);
                if (j > 0 && maze[row, j].wall[WEST] == null)
                    floodroom(maze[row, j].value, maze[row, j - 1].value);
                break;
            case 3: /* Remove walls inside same room */
                if (row > 0 && maze[row, j].wall[NORTH] != null && maze[row, j].value == maze[row - 1, j].value)
                    removeWall(row, j, NORTH);
                if (j > 0 && maze[row, j].wall[WEST] != null && maze[row, j].value == maze[row, j - 1].value)
                    removeWall(row, j, WEST);
                break;
        }
        j++;
        if (j == SIZE)
        {
            row++;
            j = 0;
            through = false;
        }
    }
}