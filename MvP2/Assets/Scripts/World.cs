﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public static World instance = null;

    public int m_worldSize = 5;
    public int m_visibleWorldRadius = 2; // Radius of the circle of loaded chunks around the player
    public int forgesBuilt = 0;

    public Chunk m_chunk;

    public Dictionary<Vector2, Chunk> instantiatedChunks = new Dictionary<Vector2, Chunk>();

    /* List of all villages in the world.
     * Key is the index in x and y of the chunk (Chunk.GetChunkIndex).
     * Value is the offset in x and y of the village within the chunk.
     */
    public Dictionary<Vector2Int, List<Vector2>> villages = new Dictionary<Vector2Int, List<Vector2>>();

    // List of chunks that have a labyrinth and whether or not they have already been completed
    public Dictionary<Vector2Int, bool> chunksWithLabyrinth = new Dictionary<Vector2Int, bool>();

    // List of all the forges created by the player (functions like villages)
    public Dictionary<Vector2Int, List<Vector2>> forges = new Dictionary<Vector2Int, List<Vector2>>(); 

    public float m_distanceBeforeDestroy = 400;

	// Use this for initialization
	void Awake()
    {
	    if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            for (int x = 0; x < m_worldSize; x++)
            {
                for (int y = 0; y < m_worldSize; y++)
                {
                    villages.Add(new Vector2Int(x, y), new List<Vector2>());
                }
            }
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update()
    {
        Vector3 playerPos = Camera.main.transform.position;
        Vector2 playerChunkCoord = Chunk.GetChunkCoordinates(playerPos.x, playerPos.z);
        Chunk c;
        for (int i = -m_visibleWorldRadius; i <= m_visibleWorldRadius; i++)
        {
            for (int j = -m_visibleWorldRadius; j <= m_visibleWorldRadius; j++)
            {
                Vector2 curChunkCoord = new Vector2(playerChunkCoord.x + i * Chunk.m_size,
                    playerChunkCoord.y + j * Chunk.m_size);
                if (!instantiatedChunks.ContainsKey(curChunkCoord))
                {
                    c = Instantiate(m_chunk, new Vector3(curChunkCoord.x, 0f, curChunkCoord.y), Quaternion.identity);
                    if (chunksWithLabyrinth.ContainsKey(Chunk.GetChunkIndex(curChunkCoord.x, curChunkCoord.y)))
                    {
                        c.hasLabyrinth = true;
                    }
                    else
                    {
                        c.hasLabyrinth = false;
                    }
                    instantiatedChunks.Add(curChunkCoord, c);
                }
            }
        }
    }
}
