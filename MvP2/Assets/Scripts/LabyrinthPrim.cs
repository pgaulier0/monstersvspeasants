﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabyrinthPrim : MonoBehaviour
{
    public float m_cellSize;
    public int size = 10;
    public Vector3 position;
    public GameObject wallPrefab;
    public GameObject floorPrefab;
    public GameObject treasurePrefab;
    public GameObject openTreasurePrefab;
    public GameObject ladderPrefab;
    public int rndSeed;

    private bool[,] m_wallsUp;
    private bool[,] m_wallsLeft;
    private List<Vector2Int> m_border;
    private List<Vector2Int> m_cells;
    private System.Random m_rnd;

    /* y (z)
     * /\
     * |
     * |
     * |
     *  --------> x (x)
     */

    // Use this for initialization
    void Start()
    {
        m_rnd = new System.Random(rndSeed);
        m_cells = new List<Vector2Int>();
        m_wallsUp = new bool[size, size];
        m_wallsLeft = new bool[size, size];
        wallPrefab.transform.localScale = new Vector3(0.6f, 3.5f, m_cellSize);
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                m_wallsUp[i, j] = true;
                m_wallsLeft[i, j] = true;
            }
        }

        int entrance = m_rnd.Next(size);
        m_cells.Add(new Vector2Int(entrance, 0));

        // For each cell in the grid
        for (int i = 0; i < size * size - 1; i++)
        {
            // Update the border
            UpdateBorder();

            // Select a random cell in the border and link it to the rest of the maze
            int randIndex = m_rnd.Next(m_border.Count);
            LinkToMaze(m_border[randIndex]);
        }

        // Instanciate Maze
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                if (m_wallsUp[x, y])
                {
                    GameObject go = Instantiate(wallPrefab, new Vector3(position.x + x * m_cellSize + m_cellSize / 2, position.y, position.z + y * m_cellSize + m_cellSize), Quaternion.Euler(0, 90, 0));
                    go.transform.SetParent(transform);
                }
                if (m_wallsLeft[x, y])
                {
                    GameObject go = Instantiate(wallPrefab, new Vector3(position.x + x * m_cellSize, position.y, position.z + y * m_cellSize + m_cellSize / 2), Quaternion.identity);
                    go.transform.SetParent(transform);
                }
                if (y == 0 && x != entrance) // First row in the bottom
                {
                    GameObject go = Instantiate(wallPrefab, new Vector3(position.x + x * m_cellSize + m_cellSize / 2, position.y, position.z), Quaternion.Euler(0, 90, 0));
                    go.transform.SetParent(transform);
                }
            }
            // Last column on the right
            GameObject g = Instantiate(wallPrefab, new Vector3(position.x + size * m_cellSize, position.y, position.z + y * m_cellSize + m_cellSize / 2), Quaternion.identity);
            g.transform.SetParent(transform);
        }
        // Add walls as foundation
        for (int n = 1; n < 10; n++)
        {
            for (int y = 0; y < size; y++)
            {
                GameObject g1 = Instantiate(wallPrefab, new Vector3(position.x, position.y - wallPrefab.transform.localScale.y * n, position.z + y * m_cellSize + m_cellSize / 2), Quaternion.identity);
                GameObject g2 = Instantiate(wallPrefab, new Vector3(position.x + size * m_cellSize, position.y - wallPrefab.transform.localScale.y * n, position.z + y * m_cellSize + m_cellSize / 2), Quaternion.identity);
                g1.transform.SetParent(transform);
                g2.transform.SetParent(transform);
            }
            for (int x = 0; x < size; x++)
            {
                GameObject g1 = Instantiate(wallPrefab, new Vector3(position.x + x * m_cellSize + m_cellSize / 2, position.y - wallPrefab.transform.localScale.y * n, position.z), Quaternion.Euler(0, 90, 0));
                GameObject g2 = Instantiate(wallPrefab, new Vector3(position.x + x * m_cellSize + m_cellSize / 2, position.y - wallPrefab.transform.localScale.y * n, position.z + size * m_cellSize), Quaternion.Euler(0, 90, 0));
                g1.transform.SetParent(transform);
                g2.transform.SetParent(transform);
            }
        }

        GameObject f = Instantiate(floorPrefab, new Vector3(position.x + size * m_cellSize / 2, position.y - wallPrefab.transform.localScale.y / 2, position.z + size * m_cellSize / 2), Quaternion.identity);
        f.transform.localScale = new Vector3(size * m_cellSize, 0.02f, size * m_cellSize);
        f.transform.SetParent(transform);

        // Pick a random cell in the top-right corner to hide the treasure
        Vector2Int treasurePos = new Vector2Int(Mathf.FloorToInt(m_rnd.Next(size / 2) + (size / 2)), Mathf.FloorToInt(m_rnd.Next(size / 2) + (size / 2)));
        Vector2Int labPos = Chunk.GetChunkIndex(position.x, position.z);
        if (position.x < 0) { labPos.x = (int)Chunk.Nfmod(labPos.x - 1, World.instance.m_worldSize); }
        if (position.z < 0) { labPos.y = (int)Chunk.Nfmod(labPos.y - 1, World.instance.m_worldSize); }
        if (!World.instance.chunksWithLabyrinth[labPos])
        {
            GameObject tr = Instantiate(treasurePrefab,
                new Vector3(position.x + treasurePos.x * m_cellSize + m_cellSize / 2, position.y - (wallPrefab.transform.localScale.y / 2) + floorPrefab.transform.localScale.y + treasurePrefab.transform.localScale.y / 2, position.z + treasurePos.y * m_cellSize + m_cellSize / 2),
                Quaternion.identity);
            tr.GetComponent<PickUpTreasure>().money = Mathf.FloorToInt(Random.value * 120) + 60;
            tr.transform.SetParent(transform);
        }
        else
        {
            GameObject tr = Instantiate(openTreasurePrefab,
                new Vector3(position.x + treasurePos.x * m_cellSize + m_cellSize / 2, position.y - (wallPrefab.transform.localScale.y / 2) + floorPrefab.transform.localScale.y + treasurePrefab.transform.localScale.y / 2, position.z + treasurePos.y * m_cellSize + m_cellSize / 2),
                Quaternion.identity);
            tr.transform.SetParent(transform);
        }

        GameObject l = Instantiate(ladderPrefab, new Vector3(position.x + entrance * m_cellSize + m_cellSize / 2, position.y - 9.4f, position.z - 0.35f), Quaternion.Euler(0, 180, 0));
        l.GetComponent<MazeEntrance>().positionToTeleport = new Vector3(position.x + entrance * m_cellSize + m_cellSize / 2, position.y, position.z + m_cellSize / 2);
        l.transform.SetParent(transform);
    }
	
	// Update is called once per frame
	void Update()
    {
		
	}

    void UpdateBorder()
    {
        m_border = new List<Vector2Int>();
        foreach (Vector2Int c in m_cells)
        {
            Vector2Int n1 = new Vector2Int(c.x + 1, c.y);
            Vector2Int n2 = new Vector2Int(c.x, c.y + 1);
            Vector2Int n3 = new Vector2Int(c.x - 1, c.y);
            Vector2Int n4 = new Vector2Int(c.x, c.y - 1);

            if (IsInGrid(n1) && !m_cells.Contains(n1) && !m_border.Contains(n1))
            {
                m_border.Add(n1);
            }
            if (IsInGrid(n2) && !m_cells.Contains(n2) && !m_border.Contains(n2))
            {
                m_border.Add(n2);
            }
            if (IsInGrid(n3) && !m_cells.Contains(n3) && !m_border.Contains(n3))
            {
                m_border.Add(n3);
            }
            if (IsInGrid(n4) && !m_cells.Contains(n4) && !m_border.Contains(n4))
            {
                m_border.Add(n4);
            }
        }
    }

    bool IsInGrid(Vector2Int c)
    {
        return c.x >= 0 && c.x < size && c.y >= 0 && c.y < size;
    }

    void LinkToMaze(Vector2Int c)
    {
        int[] xRange = { 1, 0, -1, 0 };
        int[] yRange = { 0, 1, 0, -1 };
        List<int> indexes = new List<int>(4);
        for (int i = 0; i < 4; i++)
        {
            indexes.Add(i);
        }
        for (int i = 0; i < 4; i++)
        {
            int randIndex = m_rnd.Next(indexes.Count);
            if (randIndex == indexes.Count) { randIndex = indexes.Count - 1; }
            int ri = indexes[randIndex];
            Vector2Int v = new Vector2Int(c.x + xRange[ri], c.y + yRange[ri]);
            if (IsInGrid(v) && m_cells.Contains(v))
            {
                switch (ri)
                {
                    case 0:
                        m_wallsLeft[v.x, v.y] = false;
                        m_cells.Add(c);
                        break;
                    case 1:
                        m_wallsUp[c.x, c.y] = false;
                        m_cells.Add(c);
                        break;
                    case 2:
                        m_wallsLeft[c.x, c.y] = false;
                        m_cells.Add(c);
                        break;
                    case 3:
                        m_wallsUp[v.x, v.y] = false;
                        m_cells.Add(c);
                        break;
                    default:
                        break;
                }
                return;
            }
            indexes.Remove(ri);
        }
    }

    public float GetCellSize()
    {
        return m_cellSize;
    }
}
