Projet Monsters vs Peasants
ENSEIRB-MATMECA I3 TMJV
Paul Gaulier

Contr�les:
-Se d�placer: fl�ches directionelles du clavier
-Sprinter: left shift
-Poser une forge: touche F (co�te 100 Gold)

But du jeu:
Le monde est en proie � une �pid�mie de lycantropie. La maladie se propage et pour l'en emp�cher, vous devez
fournir des armes aux villageois sans d�fenses. Poue cela, entrez dans les donjons et trouvez le tr�sor,
puis d�penser votre butin pour construire des forges. Plus vous en construisez, plus vite vos paysans s'�quiperont
et meilleures seront leurs armes !
Vous gagnez si les paysans �liminent tous les loups-garous, et vous perdez dans le cas inverse.

Notes:
Pour entrer dans un donjon, entrez en contact avec l'�chelle sur une des fa�ades.
De m�me pour r�cup�rer le tr�sor, touchez-le

Fonctionnalit�s:
-Terrain/routes g�n�r�s proc�duralement, monde torique.
-Labyrinthes g�n�r�s gr�ce � l'algorithme Prim.
-Les modifications apport�es aux chunks telles que le pillage d'un coffre dans un labyrinthe ou la pose d'une forge
sont m�moris�es: si vous rechargez le chunk aux m�mes coordonn�es ou apr�s un tour du monde, vous retrouverez 
ces modifications.
-Pareillement, les labyrinthes sont g�n�r�s avec une "seed", donc apres d�chargement et rechargement du chunk, le 
labyrinthe sera le m�me.
-Calcul de l'�volution de la population avec prise en compte du nombre de forges b�ties.
-Le nombre de forges augmente la propagation des armes mais aussi l'efficacit� des paysans arm�s contre les loups-
garous.
-Les loups-garous poursuivent les villageois.
-Les villageois arm�s poursuivent les loups-garous.
-Les villageois non arm�s se d�placent al�atoirement ou bien vers le village le plus proche.

Fonctionnalit�s manquantes:
-Les villageois ne font pas la diff�rence entre le terrain normal et les routes.
-Les collisions entre les diff�rents personnages ne fonctionnent pas (� cause du NavMeshAgent), donc les loups-garous
ne peuvent pas tuer les villageois.
-Les villageois ne fuient pas les loups-garous.