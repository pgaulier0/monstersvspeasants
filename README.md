## Projet Monsters vs Peasants
# ENSEIRB-MATMECA I3 TMJV 2017-2018
# Paul Gaulier

Contrôles:
* Se déplacer: flèches directionelles du clavier
* Sprinter: left shift
* Poser une forge: touche F (coûte 100 Gold)

But du jeu:
Le monde est en proie à une épidémie de lycantropie. La maladie se propage et pour l'en empêcher, vous devez fournir des armes aux villageois sans défenses. Poue cela, entrez dans les donjons et trouvez le trésor, puis dépenser votre butin pour construire des forges. Plus vous en construisez, plus vite vos paysans s'équiperont et meilleures seront leurs armes ! Vous gagnez si les paysans éliminent tous les loups-garous, et vous perdez dans le cas inverse.

Notes:
Pour entrer dans un donjon, entrez en contact avec l'échelle sur une des façades.
De même pour récupérer le trésor, touchez-le. 

Fonctionnalités:
* Terrain/routes générés procéduralement, monde torique.
* Labyrinthes générés grâce à l'algorithme Prim.
* Les modifications apportées aux chunks telles que le pillage d'un coffre dans un labyrinthe ou la pose d'une forge sont mémorisées: si vous rechargez le chunk aux mêmes coordonnées ou après un tour du monde, vous retrouverez ces modifications.
* Pareillement, les labyrinthes sont générés avec une "seed", donc après déchargement et rechargement du chunk, le labyrinthe sera le même.
* Calcul de l'évolution de la population avec prise en compte du nombre de forges bâties.
* Le nombre de forges augmente la propagation des armes mais aussi l'efficacité des paysans armés contre les loups-garous.
* Les loups-garous poursuivent les villageois.
* Les villageois armés poursuivent les loups-garous.
* L es villageois non armés se déplacent aléatoirement ou bien vers le village le plus proche.

Fonctionnalités manquantes:
* Les villageois ne font pas la différence entre le terrain normal et les routes.
* Les collisions entre les différents personnages ne fonctionnent pas (à cause du NavMeshAgent), donc les loups-garous ne peuvent pas tuer les villageois.
* Les villageois ne fuient pas les loups-garous